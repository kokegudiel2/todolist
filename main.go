package main

import (
	"log"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"gitlab.com/kokegudiel2/todolist/database"
	"gitlab.com/kokegudiel2/todolist/models"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	app := fiber.New()
	app.Use(cors.New())

	initDatabase()

	app.Get("/", helloWorld)
	setupRoutes(app)

	app.Listen(":8484")
}

func initDatabase() {
	var err error
	dsn := "host=lallah.db.elephantsql.com user=prwssmog password=Cnd_p1w3fGweks4tv01D5jClrdUCWgWl dbname=prwssmog port=5432"
	database.DBConn, err = gorm.Open(postgres.Open(dsn), &gorm.Config{})
	if err != nil {
		panic("Failed to connect to database!")
	}

	println("Database connected!")
	database.DBConn.AutoMigrate(&models.Todo{})
	log.Println("Migrated DB")
}

func setupRoutes(app *fiber.App) {
	app.Get("/todos", models.GetTodos)
	app.Get("/todos/:id", models.GetTodoById)
	app.Post("/todos", models.CreateTodo)
	app.Put("/todos", models.UpdateTodo)
	app.Delete("/todos/:id", models.DeleteTodo)
}

func helloWorld(c *fiber.Ctx) error {
	return c.SendString("Tumadre en 4K")
}
